const express = require('express');
const router = express.Router();
const passport = require('passport');
const jsonWebToken = require('jsonwebtoken');
const Donator = require('../models/donator'); // I am importing Donator Module (equivalent of Class)
const config = require('../config/database');




router.post('/saveInfo', (request, response, next) => {

    console.log(request.body);

    let donor = new Donator();
    donor.address = {};
    if (!request.body.firstName) {
        var result = {
            error: true,
            message: "Please insert first name!"
        }
        return response.json(result);
    } else {
        donor.firstName = request.body.firstName;
    }

    if (!request.body.lastName) {
        var result = {
            error: true,
            message: "Please insert last name!"
        }
        return response.json(result);
    } else {
        donor.lastName = request.body.lastName;
    }

    if (!request.body.contactNumber) {
        var result = {
            error: true,
            message: "Please insert contact number!"
        }
        return response.json(result);
    } else {
        donor.contactNumber = request.body.contactNumber;
    }

    if (!request.body.bloodGroup) {
        var result = {
            error: true,
            message: "Please insert blood group!"
        }
        return response.json(result);
    } else {
        donor.bloodGroup = request.body.bloodGroup.value;
    }

    if (!request.body.email) {
        var result = {
            error: true,
            message: "Please insert your email!"
        }
        return response.json(result);
    } else {
        donor.email = request.body.email;
    }

    if (!request.body.clientIp) {
        var result = {
            error: true,
            message: "Ip missing!"
        }
        return response.json(result);
    } else {
        donor.ip = request.body.clientIp;
    }

    if (!request.body.countryName) {
        var result = {
            error: true,
            message: "Country missing!"
        }
        return response.json(result);
    } else {
        donor.address.state = request.body.countryName;
    }

    if (!request.body.city) {
        var result = {
            error: true,
            message: "City missing!"
        }
        return response.json(result);
    } else {
        donor.address.city = request.body.city;
    }

    if (!request.body.latitude) {
        var result = {
            error: true,
            message: "latitude missing!"
        }
        return response.json(result);
    } else {
        donor.address.latitude = request.body.latitude;
    }

    if (!request.body.longitude) {
        var result = {
            error: true,
            message: "latitude missing!"
        }
        return response.json(result);
    } else {
        donor.address.longitude = request.body.longitude;
    }

    Donator.addDonatorInfo(donor, (errorDuringInsert, donorSaved) => {

        if (errorDuringInsert) {
            var result = {
                error: true,
                message: errorDuringInsert
            };
            return response.json(result);
        }

        donorSaved.link = donorSaved._id;

        var result = {
            error: false,
            message: "Your data saved sucessfully!",
            _id : donorSaved._id,
            link: donorSaved.link,
            firstName:donorSaved.firstName,
            lastName:donorSaved.lastName,
            contactNumber:donorSaved.contactNumber,
            email:donorSaved.email,
            bloodGroup : donorSaved.bloodGroup
        };

        Donator.update({_id:donorSaved._id}, donorSaved, function (errorDuringUpdate, newDonorSaved) {
       // Donor.addDonorInfo(donorSaved,function(error,newDonorSaved){

            if(errorDuringUpdate){ 
                return response.json({error:true,message:"could not update"});
            }

            return response.json(result);
        }); 
       
    });

});

router.post('/getBloodDonators',(request, response, next)=>{
    
    
    address = {};
    // if(request.body.city){
    //   query.address.city = request.body.city;
    // }

    // ob.search = {
    //     $and: [{
    //         $or: [{
    //             "address.street": {
    //                 $regex: new RegExp(s, 'gi')
    //             }
    //         }, {
    //             "address.city": {
    //                 $regex: new RegExp(s, 'gi')
    //             }
    //         }, {
    //             "address.zip": {
    //                 $regex: new RegExp(s, 'gi')
    //             }
    //         }]
    //     }, ob.search]
    // };


    if(request.body.countryName){
        address.country =  request.body.country
    }

    Donator.getAllDonators(address, (errorDuringFinding,donatorsFound)=>{

          if(errorDuringFinding){
              var result = {error : true, message : errorDuringFinding };
              return response.json(result);
          }

          var result = {error : false, message :"Donators found",donators : donatorsFound}
         
          return response.json(result);
    });
});

router.post('/getBloodDonatorInfo',(request, response, next)=>{

    var link ='';
    if (!request.body.link) {
        var result = {
            error: true,
            message: "Please insert link!"
        }
        return response.json(result);
    } else {
         link = request.body.link;
    }

    Donator.getDonatorById(link, function(errorFindingDonator, donatorFound){
          
           if(errorFindingDonator)
           {
                var resultNotFound = {
                    error: true,
                    message: "Couldn't find donator, error: "+errorFindingDonator
                }
               return response.json(resultNotFound);
           }

           var resultFound = {
            error: false,
            donator: donatorFound
        }
         return response.json(resultFound)
    });

});

router.post('/deleteBloodDonatorInfo',(request, response, next)=>{

    var idOfBloodDonator = '';

    if(!request.body._id)
    {
        var result = {
            error: true,
            message: "Couldn't find id!"
        }
        return response.json(result);

    }else{
        idOfBloodDonator = request.body._id 
    }
   
    Donator.deleteDonatorById(idOfBloodDonator, function(err, donatorDeleted){
          if(err)
          {
                var resultError = {
                    error: true,
                    message: "Couldn't delete!"
                }
                return response.json(resultError);
          }
          var result = {
            error: false,
            message: "Sucessfully deleted",
            _id : idOfBloodDonator,
            isDeleted : true
        }
        return response.json(result);
    });

});

router.post('/updateBloodDonatorInfo',(request, response, next)=>{
    console.log(request.body);

    let donatorWithNewProperties = {};
     
    if (!request.body._id) {

        var result = {
            error: true,
            message: "Please insert first name!"
        }
        return response.json(result);
    } else {
        donatorWithNewProperties._id = request.body._id;
    }

    if (!request.body.firstName) {
        var result = {
            error: true,
            message: "Please insert first name!"
        }
        return response.json(result);
    } else {
        donatorWithNewProperties.firstName = request.body.firstName;
    }

    if (!request.body.lastName) {
        var result = {
            error: true,
            message: "Please insert last name!"
        }
        return response.json(result);
    } else {
        donatorWithNewProperties.lastName = request.body.lastName;
    }

    if (!request.body.contactNumber) {
        var result = {
            error: true,
            message: "Please insert contact number!"
        }
        return response.json(result);
    } else {
        donatorWithNewProperties.contactNumber = request.body.contactNumber;
    }

    if (!request.body.bloodGroup) {
        var result = {
            error: true,
            message: "Please insert blood group!"
        }
        return response.json(result);
    } else {
        donatorWithNewProperties.bloodGroup = request.body.bloodGroup;
    }

    if (!request.body.email) {
        var result = {
            error: true,
            message: "Please insert your email!"
        }
        return response.json(result);
    } else {
        donatorWithNewProperties.email = request.body.email;
    }

    Donator.updateDonatorInfo (donatorWithNewProperties, function(errorDuringUpdate, newDonorUpdated){
       
        if(errorDuringUpdate){ 
            return response.json({error:true,message:"could not update"});
        }
         donatorWithNewProperties.link = donatorWithNewProperties._id;
        var result = {
            error: false,
            message: "Your data saved sucessfully!",
            donator: donatorWithNewProperties
        };
        
        return response.json(result);
    });

})

module.exports = router;