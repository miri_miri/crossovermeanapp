const express =  require('express');
const router  = express.Router();
const passport = require('passport');
const jsonWebToken = require('jsonwebtoken');
const User = require('../models/user'); // I am importing User Module (equivalent of Class)
const config =  require('../config/database');

  router.post('/register', (request, response, next) => {    
   
   // response.send('GET REGISTER');
   let newUser = new User({
    name : request.body.name,
    email : request.body.email,
    username : request.body.username,
    password : request.body.password
  });
 //  response.send('REGISTER');
   User.addUser(newUser, (error, user) => {

     if(error){
       response.json({ success : false, message : "Failed to register user" });

     }
     else
     {
       response.json({ success : true, message : "User register successfully" });
     }
   });
    
  });

  //Authenticate route
router.post('/authenticate',(request,response,next)=>{

//  response.send('Authenticate');
  const username = request.body.username;

  const password = request.body.password;

  User.getUserByUsername(username,(errorGettingUser, userFound)=>{

     if(errorGettingUser){
       throw errorGettingUser;
     }
     if(!userFound){
       return response.json({success:false, message:"User not found!"});
     }

     User.comparePassword(password, userFound.password,(errorComparingPasswords, isMatch) =>{

       if(errorComparingPasswords){
         throw errorComparingPasswords;
       }

       if(isMatch){
       
         jsonWebToken.sign(userFound._doc, config.secret,{
           expiresIn : 604800  //1 week
         },function(errorDuringGenarationOfToken,tokenGenerated){
           if(errorDuringGenarationOfToken){
             throw errorDuringGenarationOfToken;
             return;
           }

           return  response.json({
            success: true,
            token: 'JWT '+tokenGenerated,
            user:{
               id : userFound._id,
                 name:userFound.name,
                 username: userFound.username,
                 email: userFound.email
             }
        });



         });

  

    }else{

        return  response.json({ success : false,  message : "Wrong password! "  });
    }

     });

  })

});

router.get('/profile', passport.authenticate('jwt',{session : false}),   (request,response,next)=>{
    //response.send('Profile');
    response.json({user :request.user });
 });


//validate route
router.get('/validate',(request,response,next)=>{
  response.send('VALIDATE');
});



module.exports = router;