const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
const database = require('../config/database');


module.exports = function(passport){

   let options = {};
  // options.jwtFromRequest = ExtractJwt.fromAuthHeader();
   options.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');

   options.secretOrKey = database.secret;

   passport.use(new JwtStrategy(options, (jwt_payLoad, done) =>{

   // User.getUserById(jwt_payLoad._doc._id, (errorFindingUser, userFound) =>{
   User.getUserById(jwt_payLoad._id, (errorFindingUser, userFound) =>{

     if(errorFindingUser){
       return done(errorFindingUser, false);
     }

     if(userFound)
     {
        return done(null,userFound);
     }else{
       return done(null, false);
     }

    });

  }));
}
