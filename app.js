var http = require('http');
const express =  require('express');
const path = require('path');
const bodyParser =  require('body-parser'); // this parses incoming requests body. So when we submit a form we can grab the data
const cors = require('cors');  //this allows us to make a request to our api from a different domain name (by default it is blocked)
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database')

//Connect to Database
mongoose.connect(config.database.production);

//On connection with the database
mongoose.connection.on('connected',()=>{
  console.log('connected to database: '+ config.database.production);
});
// On error during connection!
mongoose.connection.on('error',()=>{
  console.log('Database error: '+ config.database.production);
});


const app = module.exports.app = express();
const usersRoute = require('./routes/usersRoute');
const bloodDonatorsRoute = require('./routes/bloodDonatorsRoute');

//Port Number
const port = 3005;

//CORS Middleware
app.use(cors());

//Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

//Body Parser Middleware
app.use(bodyParser.json());


//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

app.use('/users',usersRoute);
app.use('/bloodDonators',bloodDonatorsRoute);

app.get('/', (request, response)=>{
    
      response.send("invalid endpoint");
    
 });


 var server = http.createServer(app);
 server.listen(port, () =>{
    
    console.log('Server started on port: '+port);
    
});

var updater = require("./updater");
updater.init(server);