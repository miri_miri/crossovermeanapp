const mongoose =  require('mongoose');
const bcrypt =  require('bcryptjs');
const config =  require('../config/database');

//Create a Schema for the user
const UserSchema = mongoose.Schema({

  name : {
    type : String
  },

  email : {
    type : String,
    required : true
  },

  username : {
    type : String,
    required : true
  },

  password :{
    type : String,
    required :true
  }

});

const User = module.exports = mongoose.model('User',UserSchema);

//creating a function for user to use from outside
module.exports.getUserById = function ( userId, callback ) {
  // we can use the findById function of the mongoose
 User.findById(userId, callback)
 
}
module.exports.getUserByUsername = function ( username, callback ) {

const query = { username : username};

  // we can use the findOne function of the mongoose (object document mapper)
 User.findOne(query, callback)

}

module.exports.addUser = function (newUser, callback){
  bcrypt.genSalt(10, (errorGeneratingSalt , salt) =>{
    bcrypt.hash(newUser.password, salt, (errorGeneratingHash, hash) =>{

        if (errorGeneratingHash) {

          throw errorGeneratingHash;
          
        }

        newUser.password = hash;
        newUser.save(callback);
    });
  });
}

module.exports.comparePassword = function (passwordTypedFromUser, passwordHashed ,callback){

  bcrypt.compare(passwordTypedFromUser, passwordHashed, (errorComparingPasswords, isMatch)=>{

     if(errorComparingPasswords) throw errorComparingPasswords;

     callback(null,isMatch);

  });

}
