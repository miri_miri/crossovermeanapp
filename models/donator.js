const mongoose =  require('mongoose');
const bcrypt =  require('bcryptjs');
const config =  require('../config/database');




const DonatorSchema = mongoose.Schema({
    
      firstName : {
        type : String,
        required : true
      },
    
      lastName:{
        type : String,
        required : true
      },

      contactNumber :{
        type : String,
        required : true
      },

      email : {
        type : String,
        required : true
      },
    
      address: {
        city: String,
        street: String,
        state: String,
        zip: String,

        latitude:{
          type:Number,
          required:true,
          default: 0
         },
         longitude :{
          type:Number,
          required:true,
          default: 0
         },
      },

      // bloodGroup : {
      //      name: BloodEnum,
      //      rezus : Boolean
      // },
      bloodGroup:{
        type:String,
        required:true
      },

      ip : String,

      link:String,
    
      username : {
        type : String,
        //required : true
      },
    
      password :{
        type : String,
        //required :true
      }
    
    });
    
    const Donator = module.exports = mongoose.model('Donator',DonatorSchema);


    module.exports.addDonatorInfo = function (newDonator, callback){             
      newDonator.save(callback);          
    }

   module.exports.getAllDonators = function (query,callback){
      Donator.find(query,callback);
   }

   module.exports.getDonatorById = function(id,callback){
        Donator.findById(id,callback);
   }

   module.exports.deleteDonatorById = function(id,callback){
     Donator.findByIdAndRemove(id,callback);
   }

   module.exports.updateDonatorInfo = function(donatorWithNewProperties,callback){
    Donator.update({_id:donatorWithNewProperties._id}, donatorWithNewProperties, callback); 
   }
   
//   const BloodEnum = module.exports = Object.freeze({A: 0, B: 1, AB: 2, GroupZero : 3 });