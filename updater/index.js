(function(updater){

var socketIo = require("socket.io");

updater.init = function(server){

     var io = socketIo.listen(server);

     io.sockets.on("connection",function(socket){
         console.log("A socket was connected");
         //socket.emit("showThis", "this is from the server!");

         socket.on("newBloodDonator",function(dataFromClient){

             console.log("\nData from client io: " + JSON.stringify(dataFromClient));

             socket.broadcast.emit("BloodDonatorInfoServer",dataFromClient);

             socket.emit("BloodDonatorInfoServer",dataFromClient);
             
         }); 

         socket.on("disconnect", () => console.log("Client disconnected"));
     });

};

})(module.exports);