'use strict';


// import the moongoose helper utilities
//var utils = require('../utils');
var should = require('should');
var chaishould = require('chai').should;
var assert = require('chai').assert;
// import our Donator mongoose model
var Donator = require('../../models/donator');

describe('models: Donators', function () {
    
    
      describe('addDonatorInfo()', function (testDone) {

        it('Donator.addDonatorInfo() should add new bloodinfo', function (testDone) {
          // Create a Donator object to pass to User.addDonatorInfo()
          var newDonatorInfo =  new Donator({
            firstName : 'test first',
            lastName : 'test last name',
            contactNumber : '3894307712',
            email : 'testemail@gmail.com',
            address : { 
                city : 'test city',
                street : 'test street',
                state : 'test state',
                zip : 'test zip code',
                latitude : 43,
                longitude : 21,
             },
          
             bloodGroup : '0+',
             ip : '127:0:0:1',
             link : 'test link',
             username : 'test username',            
             password : 'test password'
          });

           Donator.addDonatorInfo(newDonatorInfo, function (errorDuringCreation, createdDonator) {
            // Confirm that that an error does not exist
            should.not.exist(errorDuringCreation);

            // verify that the returned user is what we expect
            createdDonator.firstName.should.equal('test first');
            createdDonator.lastName.should.equal('test last name');
            createdDonator.contactNumber.should.equal('3894307712');
            createdDonator.address.city.should.equal('test city');
            createdDonator.address.street.should.equal('test street');
            createdDonator.address.state.should.equal('test state');
            createdDonator.address.zip.should.equal('test zip code');
            createdDonator.address.latitude.should.equal(43);
            createdDonator.address.longitude.should.equal(21);

            createdDonator.bloodGroup.should.equal('0+');
            createdDonator.ip.should.equal('127:0:0:1');
            createdDonator.link.should.equal('test link');
            createdDonator.username.should.equal('test username');
            createdDonator.password.should.equal('test password');
          
            // Call done to tell mocha that we are done with this test
            testDone();
          });
        });


        it('Donator.getAllDonators() should return an array ',function(testDone){
            
              var address = {
                   state : "Albania"
               }
               var resultGettingAllDonors = Donator.getAllDonators(address,function(errorDuringDonatorsFetching, donatorsFetched){
                  
                     should.not.exist(errorDuringDonatorsFetching);
                                  
                     assert.typeOf(donatorsFetched,'array');

                     testDone();
               });
                 

           });
       
       
              it('Donator.getAllDonators() should return an array of type object',function(testDone){
               
                 var address = {
                      state : "Albania"
                  }
                  var resultGettingAllDonors = Donator.getAllDonators(address,function(errorDuringDonatorsFetching, donatorsFetched){
                    should.not.exist(errorDuringDonatorsFetching);
                 
                    assert.typeOf(donatorsFetched,'array');
                  
                    if(donatorsFetched.length > 0){
                        assert.typeOf(donatorsFetched[0],'object');
                    }

                    testDone()
                  });
          
              });



              it('Donator.getDonatorById() should return the donator',function(testDone){
                
                  var idOfTestDonator = '59dca798ffaf6408ecf23697'; // here I am using a test id of an document which I have saved in db!
      
               let resultFindingDonator = Donator.getDonatorById(idOfTestDonator,function(errorDuringFinding, donatorFetched){
                   
                     should.not.exist(errorDuringFinding);
                  
                     

                     testDone();
                   });
              });
             

              it("Donator.deleteDonatorById() should delete a donator" ,function(testDone){
                var idOfTestDonator = '59dca798ffaf6408ecf23697'; // here I am using a test id

                let resultFindingDonator = Donator.getDonatorById(idOfTestDonator,function(errorDuringDelete, donatorDeleted){
                    
                      should.not.exist(errorDuringDelete);
                   
                       
                      testDone();
                    });

              });

             
              it("Donator.updateDonatorInfo() should update a donator",function(testDone){
                
                var idOfTestDonator = '59dca798ffaf6408ecf23697'; // here I am using a test id
                
                let donatorWithNewProperties = {};

                donatorWithNewProperties._id = idOfTestDonator;
                donatorWithNewProperties.firstName = 'first name';
                donatorWithNewProperties.lastName = 'last name';
                donatorWithNewProperties.contactNumber = '123456789';
                donatorWithNewProperties.bloodGroup ='0+';
                donatorWithNewProperties.email = 'testemail@gmail.com';

                Donator.updateDonatorInfo(donatorWithNewProperties, function(errorDuringUpdate, donatorUpdated){
                   
                    should.not.exist(errorDuringUpdate);
                    
                                // verify that the returned user is what we expect
                   // donatorUpdated._id.should.equal(idOfTestDonator);
                   // donatorUpdated.firstName.should.equal('first name');
                   // donatorUpdated.lastName.should.equal('last name');
                    //donatorUpdated.contactNumber.should.equal('123456789');
                    //donatorUpdated.bloodGroup.should.equal('0+');
                    //donatorUpdated.email.should.equal('testemail@gmail.com');
                    
                    testDone();
                });

              });




      });
    
    
    });